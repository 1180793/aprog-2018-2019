package testespackage;

import static app.Configuracoes.*;
import app.LogErros;
import static app.Menu.*;
import static app.Utilitarios.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Formatter;

/**
 * Classe destinada ao teste de erros
 *
 * @author Gonçalo Corte Real (1180792)
 */
public class TestesUtilitarios {

    public static void main(String[] args) throws FileNotFoundException {
//
//        String email2 = "bandidos";
//        String email3 = "bandidos@oioi";
//        String email4 = "bandidos@oioi.pt";
//        String email5 = "bandidos@oioi.com.pt";
//        String email6 = "bandidos@oioi.pt.com.net";
//        System.out.println(emailValido(email2) + "; ESPERADO: false");
//        System.out.println(emailValido(email3) + "; ESPERADO: false");
//        System.out.println(emailValido(email4) + "; ESPERADO: true");
//        System.out.println(emailValido(email5) + "; ESPERADO: true");
//        System.out.println(emailValido(email6) + "; ESPERADO: false");
//        for (int i = 0; i < 100; i++) {
//            System.out.println("");
//        }
//        System.out.println("\n\nTESTES ASSOCIADOS AO MÉTODO verificaSeTemApelido() ");
//        System.out.print("\nTESTAR ‘José Costa e Silva ‘ contém ‘Costa’  ");
//        System.out.println("…" + testeVerificaSeTemApelido("José Costa e Silva ", " Costa", true));
//        System.out.print("\nTESTAR ‘José Costa e Silva ‘  não contém ‘Gomes’  ");
//        System.out.println("…" + testeVerificaSeTemApelido("José Costa e Silva ", " Gomes", false));
//        System.out.println("\n\nTESTES ASSOCIADOS AO MÉTODO apelidoELetraInicial() ");
//        System.out.print("\nTESTAR ‘José Costa e Silva retorna 'Silva J.’  ");
//        System.out.println("…" + testeApelidoELetraInicial("José Costa e Silva", "Silva J."));
        String[][] testeParticipantes = { {"Fernando", "primeiro"} , { "Bandida", "segundo"} , { "Ana", "terceira"} , { "Anita", "quarta"}  };
        for (final String[] s : testeParticipantes) {
            System.out.println(s[0] + " " + s[1]);
        }
        Arrays.sort(testeParticipantes, new Comparator<String[]>() {
            @Override
            public int compare(String[] o1, String[] o2) {
                final String time1 = o1[0];
                final String time2 = o2[0];
                return time1.compareTo(time2);
            }
            
        });
        
        for (final String[] s : testeParticipantes) {
            System.out.println(s[0] + " " + s[1]);
        }
    }

    private static int pesquisarjogoPorID(String idjogo, String[][] jogos, int nJogos) {
        for (int i = 0; i < nJogos; i++) {
            if (jogos[i][0].equals(idjogo)) {
                return i;
            }
        }
        return -1;
    }

//     /**
//     * 2. Apresenta a informação dos jogos
//     *
//     * @param nJogos - Número de jogos que indica até onde apresenta a matriz.
//     * @param jogos - Matriz com a informação dos jogos
//     */
//    public static void testevisualizarInfoJogos(int nJogos, String[] jogos) {
//        cabecalhoJogos();
//        for (int i = 0; i < nJogos; i++) {
//            String[] temp = jogos[i].split("-");
//            System.out.printf("%-8s| ", temp[0]);
//            System.out.printf("%-14s| ", temp[1]);
//            System.out.printf("%-8s", temp[2]);
//            System.out.println("");
//        }
//    }
    public static String testeVerificaSeTemApelido(String nome, String apelido, boolean resp) {
        String res = "FALHA";
        if (resp == verificaSeTemApelido(nome, apelido)) {
            res = "SUCESSO";
        }
        return res;
    }

    /**
     * * Verifica se um determinado nome contem apelido
     *
     * @param nome – nome completo
     * @param apelido – apelido a procurar
     * @return true ou false conforme o nome contem ou não o apelido
     */
    public static boolean verificaSeTemApelido(String nome, String apelido) {
        // String implements CharSequence
        return nome.contains(apelido);
    }

    /**
     * * Dado o nome completo retorna o apelido e a primeira letra .
     *
     * @param str nome completo
     * @return o String último nome seguido da 1ªletra.
     */
    public static String apelidoELetraInicial(String str) {
        str = str.trim();
        int last = str.lastIndexOf(' '); // posição do último espaço 
        // substring da posição do último espaço +1 até ao fim. 
        String resultado = str.substring(last + 1) + " " + str.substring(0, 1) + ".";
        return resultado;
    }

    public static String testeApelidoELetraInicial(String nome, String resposta) {
        String res = "FALHA";
        if (resposta.equals(apelidoELetraInicial(nome))) {
            res = "SUCESSO";
        }
        return res;
    }

    public static void calcularPremios(int nParticipantes, int nJogos, int[][] pontos, double[][] premios, String[] jogosMaxPontos, Formatter log) {
        if (nJogos != 0) {
            if (nParticipantes != 0) {
                for (int i = 0; i < nParticipantes; i++) {
                    for (int k = 0; k < nJogos; k++) {
                        int maxPontos = Integer.parseInt(jogosMaxPontos[k]);
                        int ponto = pontos[i][k];
                        if (ponto == 0) {
                            premios[i][k] = 0;
                        } else {
                            premios[i][k] = (maxPontos / ponto) * 100;
                        }
                    }
                }
            } else {
                LogErros.registarErro(log, "Impossível calcular premios. Ainda não foram carregados participantes.");
                System.out.println("Carregue um ficheiro de participantes válido para calcular os premios.");
            }
        } else {
            LogErros.registarErro(log, "Impossível calcular premios. Ainda não foram carregados jogos.");
            System.out.println("Carregue um ficheiro de jogos válido para calcular os premios.");
        }
    }
}
