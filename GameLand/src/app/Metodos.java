package app;

import static app.Configuracoes.*;
import static app.LogErros.registarEvento;
import static app.Menu.*;
import static app.Utilitarios.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Formatter;
import java.util.Scanner;

/**
 * Classe que disponibiliza os métodos especificos da aplicação
 *
 * @author Gonçalo Corte Real (1180793)
 */
public class Metodos {

    /**
     * 1. Carrega os Jogos atraves de um ficheiro com o nome 'AAAAMMDD.txt' inserido pelo utilizador
     *
     * @param nJogos - Número de jogos carregados.
     * @param nomeFichJogos - Nome do ficheiro.txt
     * @param jogos - Vetor com as informações dos jogos.
     * @param jogosId - Vetor com o id dos jogos.
     * @param jogosMaxPontos - Vetor com os pontos máximos de cada jogo.
     * @param log - Ficheiro log necessário para registar possíveis eventos e erros.
     * @return - Número de jogos carregados
     * @throws java.io.FileNotFoundException
     */
    public static int carregarJogosDoEvento(int nJogos, String nomeFichJogos, String[] jogos, String[] jogosId, String[] jogosMaxPontos, Formatter log) throws FileNotFoundException {
        int nJogosInicial = nJogos;
        try {
            Scanner fInput = new Scanner(new File(nomeFichJogos));
            int jogosCarregados = 0;
            int nLinha = 0;
            if (fInput.hasNextLine() && jogosCarregados == 0) {
                String linha = fInput.nextLine();
                if ((linha.trim()).length() > 0) {
                    nLinha++;
                    String[] temp = linha.split("-");
                    if (temp.length == N_CAMPOS_JOGOS) {
                        String id = temp[0];
                        String maxPontos = temp[2];
                        jogosId[0] = id;
                        jogosMaxPontos[0] = maxPontos;
                        jogos[jogosCarregados] = linha;
                        jogosCarregados++;
                    } else {
                        LogErros.registarErro(log, "A linha " + nLinha + " do ficheiro '" + nomeFichJogos + "' tem um número inválido de campos.");
                        System.out.println("Número inválido de campos na linha " + nLinha + ".");
                    }
                } else {
                    LogErros.registarAviso(log, "O ficheiro '" + nomeFichJogos + "' tem a linha " + nLinha + " vazia.");
                }
            }
            while (fInput.hasNextLine() && jogosCarregados < N_JOGOS) {
                String linha = fInput.nextLine();
                // Verifica se a linha não está em branco
                if ((linha.trim()).length() > 0) {
                    if (Utilitarios.jogoValido(jogosId, linha, jogosCarregados, nomeFichJogos, log)) {
                        String[] temp = linha.split("-");
                        if (temp.length == N_CAMPOS_JOGOS) {
                            String id = temp[0];
                            String maxPontos = temp[2];
                            jogosId[jogosCarregados] = id;
                            jogosMaxPontos[jogosCarregados] = maxPontos;
                            jogos[jogosCarregados] = linha;
                            jogosCarregados++;
                        } else {
                            LogErros.registarErro(log, "A linha " + nLinha + " do ficheiro '" + nomeFichJogos + "' tem um número inválido de campos.");
                            System.out.println("Número inválido de campos na linha " + nLinha + ".");
                        }
                    } else {
                        LogErros.registarErro(log, "ID de jogo repetido no ficheiro '" + nomeFichJogos + "' na linha '" + linha + "'.");
                    }
                }
            }
            fInput.close();
            // Verifica se o ficheiro está vazio
            if (jogosCarregados == 0) {
                System.out.println("Ficheiro vazio. Não foram carregados jogos.");
                LogErros.registarErro(log, "Falha ao carregar jogos. O ficheiro '" + nomeFichJogos + "' está vazio.");
                scanner.nextLine();
                return jogosCarregados;
            }
            if (jogosCarregados == N_JOGOS) {
//                System.out.println("Número máximo de jogos atingido.");
                System.out.println("Número de jogos carregados: " + jogosCarregados);
                LogErros.registarAviso(log, "Número máximo de jogos carregado: " + N_JOGOS);
                LogErros.registarEvento(log, "Foram carregados " + jogosCarregados + " jogos através do ficheiro '" + nomeFichJogos + "'.");
                scanner.nextLine();
                return jogosCarregados;
            }
            System.out.println("Número de jogos carregados: " + jogosCarregados);
            LogErros.registarEvento(log, "Foram carregados " + jogosCarregados + " jogos através do ficheiro '" + nomeFichJogos + "'.");
            scanner.nextLine();
            return jogosCarregados;
        } catch (FileNotFoundException exception) {
            // Output exception FileNotFoundExceptions.
            LogErros.registarErro(log, "Ficheiro '" + nomeFichJogos + "' não foi encontrado.");
            System.out.println("Ficheiro não encontrado.");
            scanner.nextLine();
            return 0;
        }
    }

    /**
     * 2. Apresenta a informação dos jogos
     *
     * @param nJogos - Número de jogos carregados.
     * @param jogos - Vetor com as informações dos jogos.
     */
    public static void listarInfoJogos(int nJogos, String[] jogos) {
        cabecalhoJogos();
        apresentarInfoJogos(nJogos, jogos);
    }

    public static void apresentarInfoJogos(int nJogos, String[] jogos) {
        for (int i = 0; i < nJogos; i++) {
            String[] temp = jogos[i].split("-");
            System.out.printf("%-8s| ", temp[0]);
            System.out.printf("%-14s| ", temp[1]);
            System.out.printf("%-8s", temp[2]);
            System.out.println("");
        }
    }

    /**
     * 3. Carrega informação do ficheiro de texto para memória
     *
     * @param nomeFich - nome do ficheiro que contem a informação a guardar.
     * @param info - Matriz com a informação dos participantes.
     * @param nElems - Número de participantes inscritos no evento.
     * @param log - Ficheiro log necessário para registar possíveis eventos e erros.
     * @return - Número final de elementos na matriz
     * @throws FileNotFoundException
     */
    public static int lerInfoFicheiro(String nomeFich, String[][] info, int nElems, Formatter log) throws
            FileNotFoundException {
        try {
            Scanner fInput = new Scanner(new File(nomeFich));
            int nElemsInic = nElems;
            int nLinha = 1;
            String nomeEquipa = "";
            while (fInput.hasNext() && nElems < MAX_PARTICIPANTES) {
                String linha = fInput.nextLine();
                // Verifica se linha não está em branco
                if (linha.trim().length() > 0) {
                    String[] temp = linha.split(";");
                    if (temp.length == N_CAMPOS_INFO) {
                        if (nLinha == 1) {
                            nomeEquipa = temp[3];
                        }
                        if (equipaValida(info, linha, nElems, nomeFich, nomeEquipa, log)) {
                            nElems = Utilitarios.guardarDados(nomeFich, linha, info, nElems, log);
                            nLinha++;
                        }
                    } else {
                        LogErros.registarErro(log, "Número de campos inválido na linha '" + linha + "' do ficheiro " + nomeFich);
                    }
                } else {
                    LogErros.registarAviso(log, "O ficheiro '" + nomeFich + "' tem a linha " + nLinha + " vazia.");
                }
            }
            if (nLinha != 3) {
                //System.out.println("Número de participantes inválido. (" + nLinha + ")");
                LogErros.registarErro(log, "Erro ao tentar ler o ficheiro de equipa '" + nomeFich + "'. O ficheiro não tem 3 utilizadores válidos.");
            }
            fInput.close();
            if (nElems == 0) {
                //System.out.println("Ficheiro vazio. Não foram carregados participantes.");
                LogErros.registarErro(log, "Falha ao carregar participantes. O ficheiro '" + nomeFich + "' está vazio.");
            }
            if (nElems - nElemsInic != 3) {
                nElems = nElemsInic;
                System.out.println("Erro ao ler o ficheiro '" + nomeFich + "'. Consultar '" + Configuracoes.FILE_LOGS + "' para mais informações.");
                scanner.nextLine();
            } else {
                System.out.println("Ficheiro lido com sucesso.");
                LogErros.registarEvento(log, "Ficheiro '" + nomeFich + "' lido com sucesso.");
                scanner.nextLine();
            }
            return nElems;
        } catch (FileNotFoundException exception) {
            // Output expected FileNotFoundExceptions.
            LogErros.registarErro(log, "Ficheiro '" + nomeFich + "' não foi encontrado.");
            System.out.println("Ficheiro não encontrado.");
            scanner.nextLine();
        }
        return nElems;
    }

    /**
     * 4. Visualizar toda a informação (paginada) dos participantes
     *
     * @param participantes - Matriz com a informação dos participantes.
     * @param nParticipantes - Número de participantes inscritos no evento.
     * @param log - Ficheiro log necessário para registar possíveis eventos e erros.
     */
    public static void listagemPaginada(String[][] participantes, int nParticipantes, Formatter log) {
        LogErros.registarEvento(log, "Informação dos participantes apresentada.");
        int contPaginas = 0;
        for (int i = 0; i < nParticipantes; i++) {
            if (mudarDePagina(i)) {
                if (contPaginas > 0) {
                    pausaPagina();
                }
                contPaginas++;
                System.out.println("\nPÁGINA: " + contPaginas);
                cabecalho();
            }
            for (int j = 0; j < N_CAMPOS_INFO; j++) {
                switch (j) {
                    case 1:
                        System.out.printf("%-20s| ", participantes[i][j]);
                        break;
                    case 3:
                        System.out.printf("%-14s", participantes[i][j]);
                        break;
                    default:
                        System.out.printf("%-14s| ", participantes[i][j]);
                        break;
                }
            }
            System.out.println("");
        }
    }

    /**
     * 5. Alterar dados de um participante
     *
     * @param matriz - Matriz com a informação dos participantes.
     * @param nElems - Número de participantes inscritos no evento.
     * @param log - Ficheiro log necessário para registar possíveis eventos e erros.
     * @return
     */
    public static boolean atualizarInfoParticipante(String[][] matriz, int nElems, Formatter log) {
        String email = registarString("Introduza o email do participante que pretende editar");
        int pos = Utilitarios.pesquisarElemento(email, nElems, matriz);
        if (pos > -1) {
            int op;
            do {
                System.out.println("");
                System.out.println("Dados do participante a alterar:");
                Utilitarios.mostrarParticipante(matriz[pos]);
                op = Menu.menuInfoParticipante();
                switch (op) {
                    case 0:
                        break;
                    case 1:
                        editarEmail(log, email, nElems, matriz, pos);
                        break;
                    case 2:
                        editarNome(matriz, pos, log, email);
                        break;
                    case 3:
                        editarDataNascimento(matriz, pos, log, email);
                        break;
//                    case 4:
//                        System.out.println("Nova equipa: ");
//                        matriz[pos][3] = scanner.nextLine();
//                        LogErros.registarEvento(log, "A equipa do participante '" + email + "' foi alterada para '" + matriz[pos][2] + "'.");
//                        System.out.println("Equipa alterada com sucesso.");
//                        break;
                    default:
                        System.out.println("Opção inválida.");
                        break;

                }
            } while (op != 0);
            return true;
        } else {
            System.out.printf("O participante com o email '" + email + "' não foi encontrado.");
            LogErros.registarErro(log, "Impossível editar participante com o email '" + email + "' pois este não está registado.");
            scanner.nextLine();
            return false;
        }
    }

    /**
     * Método para editar email.
     */
    public static void editarEmail(Formatter log, String email, int nElems, String[][] matriz, int pos) {
        System.out.println("Novo email: ");
        String novoEmail = scanner.nextLine();
        if (!emailValido(novoEmail)) {
            System.out.println("\nO email inserido não é válido.");
            LogErros.registarErro(log, "Não foi possível alterar o email do participante '" + email + "' para '" + novoEmail + "' porque este email não é válido");
            return;
        }
        int teste = pesquisarElemento(novoEmail, nElems, matriz);
        if (teste != -1) {
            System.out.println("\nEsse email já está registado.");
            LogErros.registarErro(log, "Não foi possível alterar o email do participante '" + email + "' para '" + novoEmail + "' porque este email já se encontra registado.");
            return;
        }
        matriz[pos][0] = novoEmail;
        LogErros.registarEvento(log, "O email do participante '" + email + "' foi alterado para '" + matriz[pos][0] + "'.");
        System.out.println("\nEmail alterado com sucesso.");
    }

    /**
     * Método para editar data de nascimento.
     */
    public static void editarDataNascimento(String[][] matriz, int pos, Formatter log, String email) throws StringIndexOutOfBoundsException {
        System.out.println("Nova data nascimento (AAAAMMDD): ");
        String novaData = scanner.nextLine();
        if (Utilitarios.verificarStringData(novaData)) {
            matriz[pos][2] = Utilitarios.formatarData(novaData);
            LogErros.registarEvento(log, "A data de nascimento do participante '" + email + "' foi alterada para '" + matriz[pos][2] + "'.");
            System.out.println("Data de nascimento alterada com sucesso.");
            return;
        }
        LogErros.registarErro(log, "Não foi possível alterar a data de nascimento do participante '" + email + "' porque foi inserida uma data inválida ('" + novaData + "').");
        System.out.println("A data inserida não é válida.");
    }

    /**
     * Método para editar nome.
     */
    public static void editarNome(String[][] matriz, int pos, Formatter log, String email) {
        System.out.println("Novo nome (Primeiro nome e apelido): ");
        String novoNome = scanner.nextLine();
        String[] nomes = novoNome.split(" ");
        if (!stringVazia(novoNome)) {
            if (nomes.length >= 2) {
                matriz[pos][1] = novoNome;
                LogErros.registarEvento(log, "O nome do participante '" + email + "' foi alterado para '" + matriz[pos][1] + "'.");
                System.out.println("\nNome alterado com sucesso.");
                return;
            } else {
                LogErros.registarErro(log, "Não foi possível alterar o nome do participante '" + email + "' porque não foi inserido um apelido.");
                System.out.println("\nNome inválido.");
            }
        } else {
            LogErros.registarErro(log, "Não foi possível alterar o nome do participante '" + email + "' porque não foi inserido um nome.");
            System.out.println("\nNão foi inserido um nome.");
        }
    }

    /**
     * 6. Carregar resultados de jogo
     *
     * @param dataEvento - Data do evento no formato AAAAMMDD.
     * @param participantes - Matriz com a informação dos participantes.
     * @param jogosId - Vetor com o id dos jogos.
     * @param nJogos - Número de jogos carregados.
     * @param nParticipantes - Número de participantes inscritos no evento.
     * @param log - Ficheiro log necessário para registar possíveis eventos e erros.
     * @param jogosMaxPontos - Vetor com os pontos máximos de cada jogo.
     * @param pontos - Matriz com os pontos de cada participante.
     * @throws java.io.FileNotFoundException
     */
    public static void carregarResultadosJogos(String dataEvento, String[][] participantes, String[] jogosId, int nJogos, int nParticipantes, int[][] pontos, String[] jogosMaxPontos, Formatter log) throws FileNotFoundException {
        boolean erro = false;
        int nLinha = 1;
        if (nJogos != 0) {
            if (nParticipantes != 0) {
                System.out.println("Introduza o ID do jogo cujos resultados pretende carregar");
                String idJogo = scanner.next();
                int jogoLinha = jogoExiste(jogosId, idJogo, nJogos);
                if (jogoLinha != -1) {
                    String nomeFich = dataEvento + "_" + idJogo + ".txt";
                    try {
                        Scanner fInput = new Scanner(new File(nomeFich));
                        while (fInput.hasNextLine()) {
                            String linha = fInput.nextLine();
                            if ((linha.trim()).length() > 0) {
                                nLinha++;
                                String[] temp = linha.split(SEPARADOR_DADOS_FICH);
                                String tempEmail = temp[0];
                                String tempPontos = temp[1];
                                if (emailValido(tempEmail)) {
                                    int teste = pesquisarEmail(tempEmail, nParticipantes, participantes);
                                    if (teste != -1) {
                                        String maxPontosString = jogosMaxPontos[jogoLinha];
                                        maxPontosString = maxPontosString.replaceAll("\\s+", "");
                                        int maxPontos = Integer.parseInt(maxPontosString);
                                        tempPontos = tempPontos.replaceAll("\\s+", "");
                                        int ponto = Integer.parseInt(tempPontos);
                                        if (ponto <= maxPontos) {
//                                            System.out.println("Max Pontos:" + maxPontos);
//                                            System.out.println("Pontos: " + ponto);
                                            for (int i = 0; i < nParticipantes; i++) {
                                                if (i == teste) {
                                                    pontos[teste][jogoLinha] = ponto;
                                                }
                                            }
                                        } else {
                                            LogErros.registarErro(log, "Número de pontos inválido na linha '" + linha + "' (Máx. pontos: " + maxPontos + ").");
                                            //System.out.println("Número de pontos inválido.");
                                            scanner.nextLine();
                                        }
                                    } else {
                                        LogErros.registarErro(log, "Email desconhecido na linha '" + linha + "' do ficheiro '" + nomeFich + "'.");
                                        //System.out.println("Email desconhecido.");
                                    }
                                } else {
                                    LogErros.registarErro(log, "Email inválido na linha '" + linha + "' do ficheiro '" + nomeFich + "'.");
                                    //System.out.println("Email inválido.");
                                }
                            } else {
                                LogErros.registarAviso(log, "O ficheiro '" + nomeFich + "' tem a linha " + nLinha + " em branco.");
                            }
                        }

                    } catch (FileNotFoundException e) {
                        erro = true;
                        // Output exception FileNotFoundException.
                        LogErros.registarErro(log, "Ficheiro '" + nomeFich + "' não foi encontrado.");
                        System.out.println("Ficheiro não encontrado.");
                        scanner.nextLine();
                    }
                } else {
                    erro = true;
                    System.out.println("O jogo com o ID '" + idJogo + "' não está carregado.");
                    LogErros.registarErro(log, "Impossível carregar resultados para o jogo '" + idJogo + "'. Não está carregado.");
                    scanner.nextLine();
                }
            } else {
                erro = true;
                LogErros.registarErro(log, "Impossível carregar resultados de jogo. Ainda não foram carregados participantes.");
                System.out.println("Carregue um ficheiro de participantes válido para carregar os resultados dos jogos.");
            }
        } else {
            erro = true;
            LogErros.registarErro(log, "Impossível carregar resultados de jogo. Ainda não foram carregados jogos.");
            System.out.println("Carregue um ficheiro de jogos válido para carregar os resultados dos jogos.");
        }
        if (!erro) {
            System.out.println("Ficheiro carregado.");
            scanner.nextLine();
        }
    }

    /**
     * 7. Faz backup da informação dos participantes e dos pontos
     *
     * @param participantes - Matriz com a informação dos participantes.
     * @param pontos - Matriz com os pontos de cada participante.
     * @param nParticipantes - Número de participantes inscritos no evento.
     * @param nJogos - Número de jogos carregados.
     * @param log - Ficheiro log necessário para registar possíveis eventos e erros.
     * @throws Exception
     */
    public static void fazerBackup(String[][] participantes, int[][] pontos, int nParticipantes, int nJogos, Formatter log) throws Exception {
        Formatter fOut = new Formatter(new File(FILE_BACKUP));
        for (int i = 0; i < nParticipantes; i++) {

            for (int j = 0; j < N_CAMPOS_INFO; j++) {
                if (j == 3) {
                    fOut.format("%s", participantes[i][j]);
                } else {
                    fOut.format("%s|", participantes[i][j]);
                }
            }
            for (int j = 0; j < nJogos; j++) {
                fOut.format(";%d", pontos[i][j]);
            }
            fOut.format("%n");
        }
        fOut.close();
        System.out.println("Backup da informação atual registado no ficheiro '" + FILE_BACKUP + "'.");
        LogErros.registarEvento(log, "Backup da informação atual registado no ficheiro '" + FILE_BACKUP + "'.");
    }

    /**
     * 8. Calcula os prémios de todos os participantes
     *
     * @param premios - Matriz dos prémios.
     * @param participantes - Matriz com a informação dos participantes.
     * @param nJogos - Número de jogos carregados.
     * @param nParticipantes - Número de participantes inscritos no evento.
     * @param pontos - Matriz com os pontos de cada participante.
     * @param jogos - Vetor com as informações dos jogos.
     * @param jogosMaxPontos - Vetor com o máximo de pontos dos jogos.
     * @param dataEvento - Data do evento no formato AAAAMMDD.
     * @param calcPremios - Boolean que indica se os prémios foram calculados desde o início da aplicação.
     * @param log - Ficheiro log necessário para registar possíveis eventos e erros.
     */
    public static boolean premios(double[][] premios, String[][] participantes, int nJogos, int nParticipantes, int[][] pontos, String[] jogos, String[] jogosMaxPontos, String dataEvento, boolean calcPremios, Formatter log) {
        if (nJogos != 0) {
            if (nParticipantes != 0) {
                resetMatrizPremios(premios);
                calcularPremiosVencedor(pontos, nParticipantes, nJogos, premios, jogos, jogosMaxPontos);
                calcularPremiosColetivos(pontos, nJogos, nParticipantes, premios);
                calcularPremiosAniversario(premios, dataEvento, participantes, nParticipantes);
                System.out.println("Prémios calculados com sucesso.");
                registarEvento(log, "Prémios calculados com sucesso.");
                calcPremios = true;
            } else {
                LogErros.registarErro(log, "Impossível calcular prémios. Ainda não foram carregados participantes.");
                System.out.println("Carregue um ficheiro de participantes válido para calcular os prémios.");
            }
        } else {
            LogErros.registarErro(log, "Impossível calcular prémios. Ainda não foram carregados jogos.");
            System.out.println("Carregue um ficheiro de jogos válido para calcular os prémios.");
        }
        return calcPremios;
    }

    /**
     * Calcula o prémio de um participante com base nos pontos obtidos
     *
     * @param pontos - Pontos do participante.
     * @param maxPontos - Número máximo de pontos do jogo.
     * @return - Prémio do participante.
     */
    public static double calcularPremio(int pontos, int maxPontos) {
        return ((double) pontos / maxPontos) * 100;
    }

    /**
     * Calcula os prémios dos vencedores
     *
     * @param pontos - Matriz com os pontos de cada participante.
     * @param nParticipantes - Número de participantes inscritos no evento.
     * @param nJogos - Número de jogos carregados.
     * @param premios - Matriz dos prémios.
     * @param jogos - Vetor com as informações dos jogos.
     * @param jogosMaxPontos - Vetor com o máximo de pontos dos jogos.
     */
    public static void calcularPremiosVencedor(int[][] pontos, int nParticipantes, int nJogos, double[][] premios, String[] jogos, String[] jogosMaxPontos) {
        int melhorPontuacao = 0, maxPontos;
        double premio;
        for (int linhaJogo = 0; linhaJogo < nJogos; linhaJogo++) {
            for (int k = 0; k < nParticipantes; k++) {
                if (pontos[k][linhaJogo] > melhorPontuacao) {
                    melhorPontuacao = pontos[k][linhaJogo];
                }
            }
            if (melhorPontuacao != 0) {
                maxPontos = Integer.parseInt(jogosMaxPontos[linhaJogo]);
                for (int i = 0; i < nParticipantes; i++) {
                    if (pontos[i][linhaJogo] == melhorPontuacao) {
                        premio = calcularPremio(pontos[i][linhaJogo], maxPontos);
                        premios[i][linhaJogo] = premios[i][linhaJogo] + premio;
                    }
                }
            }
        }
    }

    /**
     * Calcula os prémios das equipas com base nos pontos obtidos
     *
     * @param pontos - Matriz com os pontos de cada participante.
     * @param nJogos - Número de jogos carregados.
     * @param nParticipantes - Número de participantes inscritos no evento.
     * @param premios - matriz de doubles com os prémios de cada participante em cada jogo
     * @param jogosInfo - Matriz de Strings com as informaçõess de todos os jogos do evento
     */
    private static void calcularPremiosColetivos(int[][] pontos, int nJogos, int nParticipantes, double[][] premios) {
        int melhorPontuacao, pontosEquipa;
        for (int linhaJogo = 0; linhaJogo < nJogos; linhaJogo++) {
            melhorPontuacao = melhorPontuacaoEquipa(pontos, linhaJogo, nParticipantes);

            if (melhorPontuacao != 0) {
                for (int i = 0; i < nParticipantes; i = i + 3) {
                    pontosEquipa = 0;
                    for (int j = 0; j < 3; j++) {
                        pontosEquipa = pontosEquipa + pontos[i + j][linhaJogo];
                    }
                    // Atribui os 50€ de prémio aos elementos da equipa vencedora.
                    if (pontosEquipa == melhorPontuacao) {
                        for (int j = 0; j < 3; j++) {
                            premios[i + j][linhaJogo] = premios[i + j][linhaJogo] + 50;
                        }
                    }
                }
            }
        }
    }

    /**
     * Calcula a melhor pontuação de equipa obtida num jogo
     *
     * @param pontos - Matriz com os pontos de cada participante.
     * @param linhaJogo - Número da linha do vetor correspondente ao jogo.
     * @param nParticipantes - Número de participantes inscritos no evento.
     * @return - Pontuação da melhor equipa em determinado jogo.
     */
    private static int melhorPontuacaoEquipa(int[][] pontos, int linhaJogo, int nParticipantes) {
        int pontosEquipa, pontuaçaoMaxima = 0;
        for (int i = 0; i < nParticipantes; i = i + 3) {
            pontosEquipa = calcularPontosEquipa(pontos, linhaJogo, i);
            if (pontosEquipa > pontuaçaoMaxima) {
                pontuaçaoMaxima = pontosEquipa;
            }
        }
        return pontuaçaoMaxima;
    }

    /**
     * Calcula a pontuação de uma equipa 
     * NOTA: Os elementos de uma equipa encontram-se todos seguidos, agrupados 3 a 3.
     *
     * @param pontos - Matriz com os pontos de cada participante.
     * @param linhaJogo - Número da linha do vetor correspondente ao jogo.
     * @param indicePrimeiroElementoEquipa - Indica a linha da matriz do primeiro participante da equipa.
     * @return - Pontuação da equipa.
     */
    public static int calcularPontosEquipa(int[][] pontos, int linhaJogo, int indicePrimeiroElementoEquipa) {
        int pontosEquipa = 0;
        for (int i = 0; i < 3; i++) {
            pontosEquipa = pontosEquipa + pontos[indicePrimeiroElementoEquipa + i][linhaJogo];
        }
        return pontosEquipa;
    }

    /**
     * Verifica se o participante faz anos do dia do evento e calcula o prémio a receber.
     *
     * @param participantes - Matriz com a informação dos participantes.
     * @param dataEvento - Data do evento no formato AAAAMMDD.
     * @return - 0 se não fizer anos no dia do evento ou outro int qualquer que reprensta o prémio extra que recebe por fazer anos no dia do evento
     */
    public static int premiosAniversario(String[] participantes, String dataEvento) {
        String dataParticipante = converterData(participantes[2]);
        int mesParticipante = Integer.parseInt(dataParticipante.substring(4, 6));
        int diaParticipante = Integer.parseInt(dataEvento.substring(6));

        int diaEvento = Integer.parseInt(dataEvento.substring(6));
        int mesEvento = Integer.parseInt(dataEvento.substring(4, 6));
        if (mesEvento == mesParticipante && diaEvento == diaParticipante) {
            int idade = calcularIdade(dataParticipante);
            return (idade * 2);
        }
        return 0;
    }

    /**
     * Calcula os prémios de aniversário.
     *
     * @param premios - Matriz dos prémios.
     * @param dataEvento - Data do evento no formato AAAAMMDD.
     * @param participantes - Matriz com a informação dos participantes.
     * @param nParticipantes - Número de participantes inscritos no evento.
     */
    public static void calcularPremiosAniversario(double[][] premios, String dataEvento, String[][] participantes, int nParticipantes) {
        int j;
        for (int i = 0; i < nParticipantes; i++) {
            for (j = 0; j < N_JOGOS; j++) {
                if (premios[i][j] != 0) {
                    premios[i][j] = premios[i][j] + premiosAniversario(participantes[i], dataEvento);
                }
            }
        }
    }

    /**
     * 9. Elimina uma equipa
     *
     * @param participantes - Matriz com a informação dos participantes.
     * @param premios - Matriz dos prémios.
     * @param pontos - Matriz com os pontos de cada participante.
     * @param nParticipantes - Número de participantes inscritos no evento.
     * @param nJogos - Número de jogos carregados.
     * @param log - Ficheiro log necessário para registar possíveis eventos e erros.
     * @return nParticipantes com os participantes da equipa removidos.
     */
    public static int eliminarEquipa(String[][] participantes, double[][] premios, int[][] pontos, int nParticipantes, int nJogos, Formatter log) {
        if (nParticipantes != 0) {
            boolean existe = false;
            System.out.println("Introduza o nome da equipa que pretende eliminar");
            String resposta = scanner.nextLine();
            for (int i = 0; i < nParticipantes; i++) {
                if (participantes[i][3].equalsIgnoreCase(resposta)) {
                    existe = true;
                }
            }
            if (existe) {
                int valor = pesquisarEquipa(resposta, nParticipantes, participantes);
                for (int index = valor; index < valor + 3; index++) {
                    if (nParticipantes > 3) {

                        for (int colunaParticipantes = 0; colunaParticipantes < N_CAMPOS_INFO; colunaParticipantes++) {
                            participantes[index][colunaParticipantes] = participantes[index + 3][colunaParticipantes];
                            participantes[index + 1][colunaParticipantes] = participantes[index + 4][colunaParticipantes];
                            participantes[index + 2][colunaParticipantes] = participantes[index + 5][colunaParticipantes];
                        }
                        for (int colunaPontosPremios = 0; colunaPontosPremios < nJogos; colunaPontosPremios++) {
                            pontos[index][colunaPontosPremios] = pontos[index + 3][colunaPontosPremios];
                            premios[index][colunaPontosPremios] = premios[index + 3][colunaPontosPremios];
                            pontos[index + 1][colunaPontosPremios] = pontos[index + 4][colunaPontosPremios];
                            premios[index + 1][colunaPontosPremios] = premios[index + 4][colunaPontosPremios];
                            pontos[index + 2][colunaPontosPremios] = pontos[index + 5][colunaPontosPremios];
                            premios[index + 2][colunaPontosPremios] = premios[index + 5][colunaPontosPremios];
                        }
                    } else {
                        for (int colunaParticipantes = 0; colunaParticipantes < N_CAMPOS_INFO; colunaParticipantes++) {
                            participantes[index][colunaParticipantes] = "0";
                            participantes[index + 1][colunaParticipantes] = "0";
                            participantes[index + 2][colunaParticipantes] = "0";
                        }
                        for (int colunaPontosPremios = 0; colunaPontosPremios < nJogos; colunaPontosPremios++) {
                            pontos[index][colunaPontosPremios] = 0;
                            premios[index][colunaPontosPremios] = 0;
                            pontos[index + 1][colunaPontosPremios] = 0;
                            premios[index + 1][colunaPontosPremios] = 0;
                            pontos[index + 2][colunaPontosPremios] = 0;
                            premios[index + 2][colunaPontosPremios] = 0;
                        }
                    }
                }

                if (nParticipantes == 3) {
                    nParticipantes = 0;
                } else {
                    nParticipantes = nParticipantes - 3;
                }
                System.out.println("Equipa '" + resposta + "' eliminada com sucesso.");
                LogErros.registarEvento(log, "A equipa '" + resposta + "' foi eliminada.");
            } else {
                System.out.println("Essa equipa não está registada.");
                LogErros.registarErro(log, "Impossível eliminar a equipa '" + resposta + "' porque esta não está registada.");
            }
        } else {
            LogErros.registarErro(log, "Impossível eliminar equipas. Ainda não foram carregados participantes.");
            System.out.println("Carregue um ficheiro de participantes válido para eliminar uma equipa.");
        }
        return nParticipantes;
    }

    /**
     * 10. Ver prémios de equipa
     *
     * @param participantes - Matriz com a informação dos participantes.
     * @param jogos - Vetor com as informações dos jogos.
     * @param premios - Matriz dos prémios.
     * @param nParticipantes - Número de participantes inscritos no evento.
     * @param nJogos - Número de jogos carregados.
     * @param calcPremios - Boolean que indica se os prémios foram calculados desde o início da aplicação.
     * @param log - Ficheiro log necessário para registar possíveis eventos e erros.
     */
    public static void verPremiosEquipa(String[][] participantes, String[] jogos, double[][] premios, int nParticipantes, int nJogos, boolean calcPremios, Formatter log) {
        if (nJogos != 0) {
            if (nParticipantes != 0) {
                if (calcPremios == true) {
                    boolean existe = false;
                    System.out.println("Introduza o nome da equipa que pretende ver os prémios");
                    String resposta = scanner.nextLine();
                    for (int i = 0; i < nParticipantes; i++) {
                        if (participantes[i][3].equalsIgnoreCase(resposta)) {
                            existe = true;
                        }
                    }
                    if (existe) {
                        int index = pesquisarEquipa(resposta, nParticipantes, participantes);
                        System.out.println("\nEquipa: " + participantes[index][3]);
                        System.out.printf("%-15s| ", "Participantes");
                        for (int j = 0; j < nJogos; j++) {
                            if (j == nJogos - 1) {
                                System.out.printf("%-8s%n", "Jogo " + (j + 1));
                            } else {
                                System.out.printf("%-8s| ", "Jogo " + (j + 1));
                            }
                        }
                        System.out.println("==========================================================================");
                        String[][] tempParticipantes = {{participantes[index][1], "" + index}, {participantes[index + 1][1], "" + (index + 1)}, {participantes[index + 2][1], "" + (index + 2)}};
                        Arrays.sort(tempParticipantes, (String[] o1, String[] o2) -> {
                            final String time1 = o1[0];
                            final String time2 = o2[0];
                            return time1.compareTo(time2);
                        });

                        for (final String[] s : tempParticipantes) {
                            int indexPremios = Integer.parseInt(s[1]);
                            System.out.printf("%-15s| ", s[0]);
                            for (int k = 0; k < nJogos; k++) {
                                if (k == nJogos - 1) {
                                    System.out.printf("%-8s%n", df.format(premios[indexPremios][k]) + " €");
                                } else {
                                    System.out.printf("%-8s| ", df.format(premios[indexPremios][k]) + " €");
                                }
                            }
                        }
                    } else {
                        System.out.println("Essa equipa não está registada.");
                        LogErros.registarErro(log, "Impossível ver prémios da equipa '" + resposta + "' porque esta não está registada.");
                    }
                } else {
                    LogErros.registarErro(log, "Impossível ver prémios por equipa. Ainda não foram calculados prémios.");
                    System.out.println("Calcule os prémios para ver os prémios por equipa.");
                }
            } else {
                LogErros.registarErro(log, "Impossível ver prémios por equipa. Ainda não foram carregados participantes.");
                System.out.println("Carregue um ficheiro de participantes válido para ver os prémios por equipa.");
            }
        } else {
            LogErros.registarErro(log, "Impossível ver prémios por equipa. Ainda não foram carregados jogos.");
            System.out.println("Carregue um ficheiro de jogos válido para ver os prémios por equipa.");
        }
    }

    public static void verInfoJogo(String[][] participantes, String[] jogos, String[] jogosId, double[][] premios, int[][] pontos, int nParticipantes, int nJogos, boolean calcPremios, Formatter log) {
        if (nJogos != 0) {
            if (nParticipantes != 0) {
                System.out.println("Introduza o ID do jogo cujas informações pretende ver");
                String idJogo = scanner.next();
                int jogoLinha = jogoExiste(jogosId, idJogo, nJogos);
                if (jogoLinha != -1) {
                    int totalPontos = 0, contZeroPontos = 0;
                    double totalPremios = 0;
                    for (int i = 0; i < nParticipantes; i++) {
                        totalPontos = totalPontos + pontos[i][jogoLinha];
                        if (pontos[i][jogoLinha] == 0) {
                            contZeroPontos++;
                        }
                        if (calcPremios) {
                            totalPremios = totalPremios + premios[i][jogoLinha];
                        } else {
                            totalPremios = -1;
                        }
                    }
                    double mediaPerc = ((double) totalPontos / (double) nParticipantes);
                    double percDesistentes = ((double) contZeroPontos / (double) nParticipantes) * 100;
                    LogErros.registarEvento(log, "Informações detalhadas para o Jogo ID '" + idJogo + "' apresentadas");
                    System.out.println("Jogo ID: " + jogosId[jogoLinha]);
                    System.out.println("Pontuação média obtida pelos participantes: " + df.format(mediaPerc));
                    System.out.println("Percentagem de desistentes: " + df.format(percDesistentes) + "%");
                    if (totalPremios != -1) {
                        System.out.println("Total de prémios atribuídos neste jogo: " + df.format(totalPremios) + "€");
                    } else {
                        System.out.println("Para ver o total de prémios atribuídos calcule os prémios.");
                        LogErros.registarAviso(log, "Impossível mostrar o total de prémios para o Jogo ID '" + idJogo + "' dado que os prémios ainda não foram calculados.");
                    }
                    scanner.nextLine();
                } else {
                    System.out.println("O jogo com o ID '" + idJogo + "' não foi encontrado.");
                    LogErros.registarErro(log, "Impossível ver informação para o jogo '" + idJogo + "'. Não está carregado.");
                    scanner.nextLine();
                }
            } else {
                LogErros.registarErro(log, "Impossível ver informações dos jogos. Ainda não foram carregados participantes.");
                System.out.println("Carregue um ficheiro de participantes válido para ver informações dos jogos.");
            }
        } else {
            LogErros.registarErro(log, "Impossível ver informações dos jogos. Ainda não foram carregados jogos.");
            System.out.println("Carregue um ficheiro de jogos válido para ver as informações dos jogos.");
        }
    }

    public static void listagemPremios(String[][] participantes, String[] jogos, double[][] premios, int nParticipantes, int nJogos, boolean calcPremios, Formatter log) throws FileNotFoundException {
        if (nJogos != 0) {
            if (nParticipantes != 0) {
                if (calcPremios) {
                    double[][] totalPremios = new double[nParticipantes][2];
                    double tempTotalPremios = 0;
                    for (int i = 0; i < nParticipantes; i++) {
                        for (int j = 0; j < nJogos; j++) {
                            tempTotalPremios = tempTotalPremios + premios[i][j];
                        }
                        totalPremios[i][0] = i;
                        totalPremios[i][1] = tempTotalPremios;
                        tempTotalPremios = 0;
                        for (int k = 0; k < nParticipantes; k++) {
                            for (int l = k; l < nParticipantes; l++) {
                                if (totalPremios[k][1] < totalPremios[i][1]) {
                                    double temp = totalPremios[k][1];
                                    double tempIndex = totalPremios[k][0];
                                    totalPremios[k][0] = totalPremios[i][0];
                                    totalPremios[k][1] = totalPremios[i][1];
                                    totalPremios[i][0] = tempIndex;
                                    totalPremios[i][1] = temp;
                                }
                            }
                        }
                    }
                    System.out.println("Listagem de prémios para o ecrã ou guardar num ficheiro? (Ficheiro/Ecra)");
                    String resp = scanner.next();
                    if (resp.equalsIgnoreCase("ficheiro")) {
                        Formatter fOut = new Formatter(new File(FILE_PREMIOS));
                        fOut.format("%20s%n", "LISTAGEM DE PREMIOS");
                        fOut.format("%-14s| %-20s| %-14s| %-14s%n", "Email", "Nome", "Equipa", "Total Prémios");
                        fOut.format("==============|=====================|===============|===============");
                        for (int i = 0; i < nParticipantes; i++) {
                            Double valor = totalPremios[i][0];
                            int index = valor.intValue();
                            String nome = participantes[index][1];
                            fOut.format("\n");
                            fOut.format("%-14s| ", participantes[index][0]);
                            fOut.format("%-20s| ", apelidoELetraInicial(nome));
                            fOut.format("%-14s| ", participantes[index][3]);
                            fOut.format("%-14s", df.format(totalPremios[i][1]) + "€");
                        }
                        fOut.format("\n");
                        fOut.format("%68s", "Data de elaboração da lista: " + timestamp());
                        scanner.nextLine();
                        fOut.close();
                        System.out.println("\nListagem dos prémios registada no ficheiro '" + FILE_PREMIOS + "'.");
                        LogErros.registarEvento(log, "Listagem dos prémios registada no ficheiro '" + FILE_PREMIOS + "'.");
                    } else if (resp.equalsIgnoreCase("ecra")) {
                        System.out.printf("%20s%n", "LISTAGEM DE PREMIOS");
                        System.out.printf("%-14s| %-20s| %-14s| %-14s%n", "Email", "Nome", "Equipa", "Total Prémios");
                        System.out.println("==============|=====================|===============|===============");
                        for (int i = 0; i < nParticipantes; i++) {
                            Double valor = totalPremios[i][0];
                            int index = valor.intValue();
                            String nome = participantes[index][1];
                            System.out.printf("%-14s| ", participantes[index][0]);
                            System.out.printf("%-20s| ", apelidoELetraInicial(nome));
                            System.out.printf("%-14s| ", participantes[index][3]);
                            System.out.printf("%-14s", df.format(totalPremios[i][1]) + "€");
                            System.out.println("");
                        }
                        System.out.println("");
                        System.out.printf("%68s", "Data de elaboração da lista: " + timestamp());
                        LogErros.registarEvento(log, "Listagem de prémios apresentada.");
                        scanner.nextLine();
                    } else {
                        System.out.println("Resposta inválida.");
                        scanner.nextLine();
                    }
                } else {
                    LogErros.registarErro(log, "Impossível ver listagem dos prémios. Ainda não foram calculados prémios.");
                    System.out.println("Calcule os prémios dos jogos para ver a listagem dos prémios.");
                }
            } else {
                LogErros.registarErro(log, "Impossível ver listagem dos prémios. Ainda não foram carregados participantes.");
                System.out.println("Carregue um ficheiro de participantes válido para ver a listagem dos prémios.");
            }
        } else {
            LogErros.registarErro(log, "Impossível ver listagem dos prémios. Ainda não foram carregados jogos.");
            System.out.println("Carregue um ficheiro de jogos válido para ver a listagem dos prémios.");
        }
    }
}
