package app;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;

/**
 * Classe que disponibiliza métodos associados à criação e escrita de informação num ficheiro de registo de erros.
 * @author Gonçalo Corte Real (1180793)
 */

public class LogErros {
    
    /**
     * Cria e abre um ficheiro de texto para escrita
     * @param nomeFile - Nnome do ficheiro log a criar
     * @return Formatter que referencia o ficheiro log criado
     * @throws FileNotFoundException
     */
    public static Formatter criarEAbrirFileLogErros(String nomeFile) throws FileNotFoundException {
        Formatter outputLogErros = new Formatter(new File(nomeFile));
        return outputLogErros;
    }

    /**
     * Regista no ficheiro o inicio da aplicaçãp
     * @param log - Referência ao ficheiro log
     */
    public static void registarInicio(Formatter log) {
        log.format("[%-2s] EVENTO: GAMELAND INICIADO%n", Utilitarios.timestamp());
    }
    
    /**
     * Regista no ficheiro o fecho da aplicaçãp
     * @param log - Referência ao o ficheiro log
     */
    public static void registarFecho(Formatter log){
        log.format("[%-2s] EVENTO: GAMELAND TERMINADO%n", Utilitarios.timestamp());
    }
    
    /**
     * Regista um evento no ficheiro log
     * @param log Referência ao o ficheiro log
     * @param evento Evento a registar
     */
    public static void registarEvento(Formatter log, String evento) {
        log.format("[%-2s] EVENTO: %s%n", Utilitarios.timestamp(), evento);
    }
    
    /**
     * Regista um evento no ficheiro log
     * @param log Referência ao o ficheiro log
     * @param opcao Opção a registar
     */
    public static void registarOpcao(Formatter log, String opcao) {
        log.format("[%-2s] MENU: %s%n", Utilitarios.timestamp(), opcao);
    }
    
    /**
     * Regista um evento no ficheiro log
     * @param log - Referência para o ficheiro log
     * @param mensagem Mensagem a registar
     */
    public static void registarMensagem(Formatter log, String mensagem){
        log.format("[%-2s] %s%n", Utilitarios.timestamp(), mensagem);
    }
    
    /**
     * Regista um evento no ficheiro log
     * @param log - Referência para o ficheiro log
     * @param aviso Aviso a registar
     */
    public static void registarAviso(Formatter log, String aviso){
        log.format("[%-2s] AVISO: %s%n", Utilitarios.timestamp(), aviso);
    }
    
    /**
     * Regista um evento no ficheiro log
     * @param log - Referência para o ficheiro log
     * @param erro Erro a registar
     */
    public static void registarErro(Formatter log, String erro){
        log.format("[%-2s] ERRO: %s%n", Utilitarios.timestamp(), erro);
    }

    /**
     * Fechar o ficheiro de registo de erros
     * @param outputLogErros - referência para o ficheiro de erros
     */
    public static void fecharFileLogErros(Formatter outputLogErros) {
        outputLogErros.close();
    }

    /**
     * Listar para o ecrã um ficheiro de texto
     * @param nomeFile - nome do ficheiro de texto
     * @throws FileNotFoundException
     */
    public static void listarFileLogErros(String nomeFile) throws FileNotFoundException {
        Scanner outputLogErros = new Scanner(new File(nomeFile));
        while (outputLogErros.hasNextLine()) {
            System.out.println(outputLogErros.nextLine());
        }
        outputLogErros.close();
    }
}
