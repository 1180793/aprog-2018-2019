package app;

import static app.Configuracoes.*;
import static app.Utilitarios.*;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Classe do menu da aplicação
 *
 * @author Gonçalo Corte Real (1180793)
 */
public class Menu {

    /**
     * Menu da aplicação GameLand
     *
     * @return - Opção do menu.
     */
    public static int abrirMenu() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("|------------------------------------------------------|");
        System.out.println("|       Game Land Party Games | " + Utilitarios.mostrarData() + "       |");
        System.out.println("|------------------------------------------------------|");
        System.out.println("| 1: Carregar informação de jogos                      |");
        System.out.println("| 2: Ver detalhes sobre os jogos                       |");
        System.out.println("| 3: Ler ficheiro de inscrição de equipa               |");
        System.out.println("| 4: Ver detalhes dos participante                     |");
        System.out.println("| 5: Alterar dados de um participante                  |");
        System.out.println("| 6: Carregar resultados de jogo                       |");
        System.out.println("| 7: Fazer backup da informação                        |");
        System.out.println("| 8: Calcular prémios                                  |");
        System.out.println("| 9: Remover equipa                                    |");
        System.out.println("| 10: Ver prémios por equipa                           |");
        System.out.println("| 11: Ver informação de jogo                           |");
        System.out.println("| 12: Ver listagem de prémios                          |");
        System.out.println("| 0: Terminar programa                                 |");
        System.out.println("|------------------------------------------------------|");
        try {
            int op = scanner.nextInt();
            return op;
        } catch (InputMismatchException e) {
            int op = 100;
            return op;
        }
    }

    /**
     * Mostra o menu com as informações do Deputado
     *
     * @return Opção escolhida
     */
    public static int menuInfoParticipante() {
        String texto = "|------------------------|\n"
                + "| Parâmetro a editar:    |\n"
                + "| 1: Email               |\n"
                + "| 2: Nome                |\n"
                + "| 3: Data de Nascimento  |\n"
                //                +    "| 4: Equipa              |\n"
                + "| 0: Terminar            |\n"
                + "|------------------------|"
                + "\nInsira a opção: \n";
        System.out.printf("%n%s%n", texto);
        int op = scanner.nextInt();
        scanner.nextLine();
        return op;
    }

    /**
     * Pausa o menu.
     */
    public static void pausa() {
        System.out.println("\n\nPara continuar digite ENTER\n");
        scanner.nextLine();
    }

    /**
     * Cabeçalho nova página
     */
    public static void cabecalho() {
        System.out.printf("%37s%n", "PARTICIPANTES");
        System.out.printf("%-14s| %-20s| %-14s| %-14s%n", "Email", "Nome", "Nascimento", "Equipa");
        System.out.printf("%50s%n", "===============================================================");
    }

    /**
     * Cabeçalho nova página
     */
    public static void cabecalhoJogos() {
        System.out.printf("%17s%n", "JOGOS");
        System.out.printf("%-8s| %-14s| %-8s%n", " ID", "Tipo de Jogo", "Max Pontos");
        System.out.printf("%20s%n", "=======================================");
    }

    /**
     * Pausa entre paginação de elementos
     */
    public static void pausaPagina() {
        System.out.println("\n\nPara continuar para a próxima página digite ENTER\n");
        scanner.nextLine();
    }

    /**
     * Método para fechar o menu
     * @param scanner - Scanner.
     * @param op - Opção a registar.
     * @param log - Ficheiro log necessário para registar possíveis eventos e erros.
     * @return op - Opção registada
     */
    public static int fecharMenu(Scanner scanner, int op, Formatter log) {
        System.out.println("Já fez todas as gravações necessárias? Escreva 'S' para terminar.");
        char resp = (scanner.next()).charAt(0);
        if (resp != 'S' && resp != 's') {
            op = 1;
            return op;
        } else {
            System.out.println("A sair do programa...");
            LogErros.registarFecho(log);
            LogErros.fecharFileLogErros(log);
            return op;
        }
    }
}
