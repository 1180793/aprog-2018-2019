package app;

/**
 * Classe que disponibiliza constantes da aplicação
 * @author Gonçalo Corte Real (1180793)
 */
public class Configuracoes {
    
    //Configuração - Constantes da Aplicação
    public final static int MAX_PARTICIPANTES = 30;
    public final static int N_CAMPOS_INFO = 4;
    public final static int N_CAMPOS_JOGOS = 3;
    public final static int N_JOGOS = 6;
    public final static int MAX_LINHAS_PAGINA = 3;
    public final static String SEPARADOR_DADOS_FICH = ";";
    public final static String FILE_LOGS = "Logs.txt";
//    public final static String FILE_LOG_ERROS_INSCRICOES = "ErrosInscricoes.txt";
//    public final static String FILE_LOG_ERROS_PONTUACOES = "ErrosPontos.txt";
    public final static String FILE_BACKUP = "backup.txt";
    public final static String FILE_PREMIOS = "premios.txt";
}
