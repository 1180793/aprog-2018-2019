package app;

import static app.Configuracoes.*;
import static app.Menu.*;
import static app.Metodos.*;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;
import static testespackage.TestesUtilitarios.*;

/**
 * APROG - Aplicação Final 2018
 * @name GameLand
 * @author Gonçalo Corte Real (1180793)
 */
public class Main {

    //Estrutura de dados
    public static String[] jogos = new String[N_JOGOS];
    public static String[] jogosId = new String[N_JOGOS];
    public static String[] jogosMaxPontos = new String[N_JOGOS];
    public static String[][] participantes = new String[MAX_PARTICIPANTES][N_CAMPOS_INFO];
    public static int nParticipantes = 0;
    public static double[][] premios = new double[MAX_PARTICIPANTES][N_JOGOS];
    public static int[][] pontos = new int[MAX_PARTICIPANTES][N_JOGOS];
    public static String dataEvento;
    public static int nJogos = 0;
    public static boolean calcPremios = false;
    

    public static void main(String[] args) throws FileNotFoundException, Exception {
        Formatter log = LogErros.criarEAbrirFileLogErros(FILE_LOGS);
        LogErros.registarInicio(log);
        Scanner scanner = new Scanner(System.in);
        int op;
        do {
            op = abrirMenu();
            switch (op) {
                case 1:
                    // COMPLETO: 1. Carregar jogos atraves do ficheiro AAAAMMDD.txt, definido pelo utilizador. ID validado. 
                    dataEvento = Utilitarios.registarData(log, "Qual a data do evento (AAAAMMDD)?");
                    if (!dataEvento.equalsIgnoreCase("-1")) {
                        nJogos = carregarJogosDoEvento(nJogos, dataEvento + ".txt", Main.jogos, Main.jogosId, Main.jogosMaxPontos, log);
                        pausa();
                        break;
                    }
                    System.out.println("Erro ao carregar o ficheiro de jogos.");
                    pausa();
                    break;
                case 2:
                    // COMPLETO: 2. Visualizar informações sobre os jogos.
                    if (Utilitarios.erroSemJogos(nJogos)) {
                        System.out.println("Carregue um ficheiro válido para visualizar as informações sobre os jogos desse dia.");
                        LogErros.registarErro(log, "Impossível visualizar as informações sobre os jogos. Não foi carregado um ficheiro válido.");
                        pausa();
                        break;
                    }
                    listarInfoJogos(nJogos, Main.jogos);
                    LogErros.registarEvento(log, "Informação dos jogos carregados apresentada.");
                    pausa();
                    break;
                case 3:
                    // 3. Ler de um ficheiro a informaçãos dos participantes - COMPLETO; Todo o tipo de erros cobertos
                    String nomeFich = Utilitarios.registarString("Qual o nome do ficheiro de equipa?");
                    nParticipantes = Metodos.lerInfoFicheiro(nomeFich + ".txt", Main.participantes, Main.nParticipantes, log);
                    pausa();
                    break;
                case 4:
                    // 4. Lista paginada com informação dos participantes.
                    if (Utilitarios.erroSemJogos(nParticipantes)) {
                        System.out.println("Carregue um ficheiro válido para visualizar as informações sobre as equipas.");
                        LogErros.registarErro(log, "Impossível visualizar as informações sobre as equipas. Não foram carregados ficheiros válidos.");
                        pausa();
                        break;
                    }
                    Metodos.listagemPaginada(Main.participantes, Main.nParticipantes, log);
                    pausa();
                    break;
                case 5:
                    if (Utilitarios.erroSemJogos(nParticipantes)) {
                        System.out.println("Carregue um ficheiro válido para editar as informações sobre os participantes.");
                        LogErros.registarErro(log, "Impossível editar as informações sobre os participantes. Não foram carregados ficheiros de partcipantes.");
                        pausa();
                        break;
                    }
                    atualizarInfoParticipante(Main.participantes, Main.nParticipantes, log);
                    pausa();
                    break;
                case 6:
                    carregarResultadosJogos(Main.dataEvento, Main.participantes, Main.jogosId, Main.nJogos, Main.nParticipantes, Main.pontos, Main.jogosMaxPontos, log);
                    pausa();
                    break;
                case 7:
                    fazerBackup(Main.participantes, Main.pontos, Main.nParticipantes, Main.nJogos, log);
                    pausa();
                    break;
                case 8:
                    calcPremios = premios(Main.premios, Main.participantes, Main.nJogos, Main.nParticipantes, Main.pontos, Main.jogos, Main.jogosMaxPontos, Main.dataEvento, Main.calcPremios, log);
                    pausa();
                    break;
                case 9:
                    nParticipantes = eliminarEquipa(Main.participantes, Main.premios, Main.pontos, Main.nParticipantes, Main.nJogos, log);
                    pausa();
                    break;
                case 10:
                    verPremiosEquipa(Main.participantes, Main.jogos, Main.premios, Main.nParticipantes, Main.nJogos, Main.calcPremios, log);
                    pausa();
                    break;
                case 11:
                    verInfoJogo(Main.participantes, Main.jogos, Main.jogosId, Main.premios, Main.pontos, Main.nParticipantes, Main.nJogos, Main.calcPremios, log);
                    pausa();
                    break;
                case 12:
                    listagemPremios(Main.participantes, Main.jogos, Main.premios, Main.nParticipantes, Main.nJogos, Main.calcPremios, log);
                    pausa();
                    break;
//                TESTE: Ver matriz dos prémios
//                case 13:
//                    for (int i = 0; i < nParticipantes; i++) {
//                        String temp = "";
//                        for (int j = 0; j < nJogos; j++) {
//                            temp = temp + premios[i][j] + " | ";
//                        }
//                        System.out.println(temp);
//                    }
//                    break;
                case 0:
                    op = fecharMenu(scanner, op, log);
                    break;
                default:
                    System.out.println("Opção incorreta. Repita");
                    LogErros.registarErro(log, "Opção inválida. (Opção '" + op + "')");
                    pausa();
                    break;
            }
        } while (op != 0);
    }

}
