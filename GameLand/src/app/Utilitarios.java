package app;

import static app.Configuracoes.MAX_LINHAS_PAGINA;
import static app.Configuracoes.N_CAMPOS_INFO;
import static app.Configuracoes.N_CAMPOS_JOGOS;
import static app.Configuracoes.SEPARADOR_DADOS_FICH;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Classe que disponibiliza métodos utilitários da aplicação
 *
 * @author Gonçalo Corte Real (1180793)
 */
public class Utilitarios {

    /**
     * Scanner
     */
    public static Scanner scanner = new Scanner(System.in);

    /**
     * Decimal Format
     */
    public static DecimalFormat df = new DecimalFormat("#0.00"); 
    
    /**
     * Escreve a data do sistema no ecrã
     *
     * @return Data em string.
     */
    public static String mostrarData() {
        String date = new SimpleDateFormat("HH:mm dd/MM/yyyy").format(Calendar.getInstance().getTime());
        return date;
    }
    
    /**
     * Hora e data do sistema em string
     *
     * @return - Timestamp em string.
     */
    public static String timestamp() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String timeStamp = sdf.format(new Date());
        return timeStamp;
    }

    /**
     * Regista uma String com base numa String
     *
     * @param log - Ficheiro log necessário para registar possíveis eventos e erros.
     * @param pergunta - String que indica o valor que é suposto introduzir.
     * @return - Data em String.
     */
    public static String registarData(Formatter log, String pergunta) {
        int data;
        System.out.println(pergunta);
        String resposta = scanner.next();
        try {
            data = Integer.parseInt(resposta);
            return resposta;
        } catch (InputMismatchException e) {
            LogErros.registarErro(log, "Erro ao registar o ficheiro. Nome inválido");
            return "-1";
        }
    }

    /**
     * Regista uma String com base numa String
     *
     * @param pergunta - String que indica o valor que é suposto introduzir.
     * @return - Resposta em String.
     */
    public static String registarString(String pergunta) {
        System.out.println(pergunta);
        String resposta = scanner.next();
        return resposta;
    }

    /**
     * Regista um Inteiro com base numa String
     *
     * @param pergunta - String que indica o valor que é suposto introduzir.
     * @return - Resposta em inteiro.
     */
    public static int registarInt(Formatter log, String pergunta) throws InputMismatchException {
        System.out.println(pergunta);
        try {
            int resposta = scanner.nextInt();
            return resposta;
        } catch (InputMismatchException e) {
            LogErros.registarErro(log, "Erro ao registar o ficheiro. Nome inválido");
        }
        return -1;
    }

    /**
     * Aux. Verifica se foram carregados jogos
     *
     * @param nJogos
     * @return valor boolean da expressão nJogos == 0
     */
    public static boolean erroSemJogos(int nJogos) {
        return nJogos == 0;
    }

    /**
     * Aux. Valida o id de cada jogo na funcionalidade 1.
     *
     * @param jogosId - Array com a lista dos IDs de jogos registados até ao momento.
     * @param linha - Linha do ficheiro a analizar.
     * @param jogosCarregados - Número de jogos carregados até ao momento.
     * @return - Boolean que indica se o jogo é ou não válido.
     */
    public static boolean jogoValido(String[] jogosId, String linha, int jogosCarregados, String nomeFich, Formatter log) {
        boolean jogoValido = true;
        String[] temp = linha.split("-");
        if (temp.length == N_CAMPOS_JOGOS) {
            String id = temp[0];
            for (int i = 0; i < (jogosCarregados + 1); i++) {
                if (id.equalsIgnoreCase(jogosId[i])) {
                    jogoValido = false;
                }
            }
        } else {
            LogErros.registarErro(log, "Número de campos inválido na linha '" + linha + "' do ficheiro " + nomeFich);
            jogoValido = false;
        }
        return jogoValido;
    }

    /**
     * Aux. Valida a equipa a registar na funcionalidade 3.
     *
     * @param participantes - Matriz com a informação dos participantes.
     * @param linha - Linha a processar.
     * @param nParticipantes - Número de participantes inscritos no evento.
     * @param nomeFich - Nome do ficheiro da equipa.
     * @param nomeEquipa - Nome da equipa a carregar.
     * @param log - Ficheiro log necessário para registar possíveis eventos e erros.
     * @return - Se a equipa é válida ou não.
     */
    public static boolean equipaValida(String[][] participantes, String linha, int nParticipantes, String nomeFich, String nomeEquipa, Formatter log) {
        boolean equipaValida = true;
        String[] temp = linha.split(";");
        String email = temp[0];
        String equipa = temp[3];
        int pos = pesquisarEmail(email, nParticipantes, participantes);
        if (pos == -1) {
            if (equipa.equalsIgnoreCase(nomeEquipa)) {
                equipaValida = true;
            } else {
                LogErros.registarErro(log, "Nomes das equipas no ficheiro '" + nomeFich + "' não coincidem.");
                equipaValida = false;
            }

        } else {
            LogErros.registarErro(log, "Email inválido na linha '" + linha + "' do ficheiro " + nomeFich);
            equipaValida = false;
        }
        return equipaValida;
    }

    /**
     * Aux. Acede à informação de uma linha do ficheiro e guarda-a na estrutura dados se ainda não existe linha com aquele valor no 1º elemento.
     *
     * @param nomeFich - Nome do ficheiro de participantes.
     * @param linha - Linha a processar.
     * @param info - Matriz com a informação dos participantes.
     * @param nElems - Número de participantes inscritos no evento.
     * @return - Novo número de elementos da matriz.
     */
    public static int guardarDados(String nomeFich, String linha, String[][] info, int nElems, Formatter log) {
        String[] temp = linha.split(SEPARADOR_DADOS_FICH);
        if (temp.length == N_CAMPOS_INFO) {
            String email = temp[0].trim();
            String nome = temp[1].trim();
            String[] nomes = nome.split(" ");
            int pos = pesquisarElemento(email, nElems, info);
            if (pos == -1) {
                if (emailValido(email)) {
                    if (nomes.length >= 2) {
                        for (int i = 0; i < N_CAMPOS_INFO; i++) {
                            info[nElems][i] = temp[i].trim();
                        }
                        nElems++;
                    } else {
                        LogErros.registarErro(log, "Nome inválido no ficheiro '" + nomeFich + "' (" + nome + ").");
                    }
                } else {
                    LogErros.registarErro(log, "Email inválido no ficheiro '" + nomeFich + "' (" + email + ").");
                    //System.out.println("Email inválido '" + num + "'.");
                }
            } else {
                LogErros.registarErro(log, "Email repetido no ficheiro '" + nomeFich + "' (" + email + ").");
                //System.out.println("Email repetido '" + num + "'.");
            }
        } else {
            //System.out.println("Número de campos inválido na linha '" + linha + "'.");
            LogErros.registarErro(log, "Número de campos inválido no ficheiro '" + nomeFich + "' na linha '" + linha + "'.");
        }
        return nElems;
    }

    /**
     * Aux. Pesquisar linha de matriz por primeiro elemento da linha.
     * Funcionalidade 3 - Verifica se não existem emails repetidos no ficheiro dos participantes.
     * Funcionalidade 5 - Verifica se existe o email email do participante a tentar editar.
     *
     * @param valor - Valor a pesquisar.
     * @param nEl - Número de participantes inscritos no evento.
     * @param mat - Matriz com a informação dos participantes.
     * @return -1 se não existe linha com esse valor ou o nº da linha cujo primeiro elemento é esse valor
     */
    public static int pesquisarElemento(String valor, int nEl, String[][] mat) {
        for (int i = 0; i < nEl; i++) {
            if (mat[i][0].equals(valor)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Aux. Pesquisar linha de matriz por primeiro elemento da linha. Funcionalidade 3 - Verifica se não existem emails repetidos no ficheiro dos participantes. Funcionalidade 5 - Verifica se existe o email email do participante a tentar editar.
     *
     * @param valor - elemento a pesquisar
     * @param nEl - nº de elementos da matriz
     * @param mat - matriz com a informação
     * @return -1 se não existe linha com esse valor ou o nº da linha cujo primeiro elemento é esse valor
     */
    public static int pesquisarEquipa(String valor, int nEl, String[][] mat) {
        for (int i = 0; i < nEl; i++) {
            if (mat[i][3].equalsIgnoreCase(valor)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Aux. Mostra as colunas com a informação relativa ao participante daquela linha
     *
     * @param participante - Linha da matriz correspondente ao participante.
     */
    public static void mostrarParticipante(String participante[]) {
        for (int i = 0; i < N_CAMPOS_INFO; i++) {
            switch (i) {
                case 1:
                    System.out.printf("%-20s| ", participante[i]);
                    break;
                case 3:
                    System.out.printf("%-14s", participante[i]);
                    break;
                default:
                    System.out.printf("%-14s| ", participante[i]);
                    break;
            }
        }
    }
    
    /**
     * Retorna o apelido e a primeira letra de um nome.
     *
     * @param str - Nome completo.
     * @return - String com o último nome seguido da 1ªletra.
     */
    public static String apelidoELetraInicial(String str) {
        str = str.trim();
        int last = str.lastIndexOf(' '); // posição do último espaço 
        // substring da posição do último espaço +1 até ao fim. 
        String resultado = str.substring(last + 1) + " " + str.substring(0, 1) + ".";
        return resultado;
    }

    /**
     * Aux. Verifica se a string não é nula e se não está vazia
     *
     * @param string String a verificar.
     * @return true ou false.
     */
    public static boolean stringVazia(String string) {
        return ((string == null) || (string.isEmpty()));
    }

    /**
     * Converter data no formato dd/mm/ano em AAAAMMDD
     *
     * @param data - Data no formato dd/mm/ano.
     * @return data no formato AAAAMMDD.
     */
    public static String converterData(String data) {
        String[] aux = data.trim().split("/");
        String dia = aux[0].length() < 2 ? "0" + aux[0] : aux[0];
        String mes = aux[1].length() < 2 ? "0" + aux[1] : aux[1];
        return aux[2] + mes + dia;
    }

    /**
     * Calcula a idade inserida a data de nascimento
     *
     * @param dataNasc Data de nascimento em AAAAMMDD
     * @return Idade
     */
    public static int calcularIdade(String dataNasc) {
        int ano = Integer.parseInt(dataNasc.substring(0, 4));
        int mes = Integer.parseInt(dataNasc.substring(4, 6));
        int dia = Integer.parseInt(dataNasc.substring(6));

        int anoAtual = Calendar.getInstance().get(Calendar.YEAR);
        int mesAtual = Calendar.getInstance().get(Calendar.MONTH) + 1;
        int diaAtual = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

        int idade = anoAtual - ano;
        if (mes == mesAtual) {
            if (dia < diaAtual) {
                idade--;
            }
        }
        return idade;
    }

    /**
     * Formata a data para um formato legível
     *
     * @param desformatada Data em AAAAMMDD
     * @return Data em DD/MM/AAAA
     */
    public static String formatarData(String desformatada) {
        String ano = desformatada.substring(0, 4);
        String mes = desformatada.substring(4, 6);
        String dia = desformatada.substring(6);

        return (dia + "/" + mes + "/" + ano);
    }

    /**
     * Verifica se a string pode contém uma data válida dp tipo AAAAMMDD
     *
     * @param string - String a verificar
     * @return True ou False
     */
    public static boolean verificarStringData(String string) throws StringIndexOutOfBoundsException {
        if (isNumber(string)) {
            try {
                int ano = Integer.parseInt(string.substring(0, 4));
                int mes = Integer.parseInt(string.substring(4, 6));
                int dia = Integer.parseInt(string.substring(6));
                int anoAtual = Calendar.getInstance().get(Calendar.YEAR);
                int mesAtual = Calendar.getInstance().get(Calendar.MONTH) + 1;
                int diaAtual = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
                if ((ano < anoAtual) || ((ano == anoAtual) && (mes < mesAtual)) || ((ano == anoAtual) && (mes == mesAtual) && (dia <= diaAtual))) {
                    if ((mes >= 1) && (mes <= 12)) {
                        switch (mes) {
                            case 1:
                            case 3:
                            case 5:
                            case 7:
                            case 8:
                            case 10:
                            case 12:
                                if ((dia >= 1) && (dia <= 31)) {
                                    return true;
                                }
                                break;
                            case 4:
                            case 6:
                            case 9:
                            case 11:
                                if ((dia >= 1) && (dia <= 310)) {
                                    return true;
                                }
                                break;
                            case 2:
                                if ((dia >= 1) && (dia <= 29) && !(verificarAnoBissexto(ano))) {
                                    return true;
                                } else {
                                    if ((dia >= 1) && (dia <= 29) && !(verificarAnoBissexto(ano))) {
                                        return true;
                                    }
                                }
                                break;
                        }
                    }
                }
            } catch (StringIndexOutOfBoundsException e) {
                return false;
            }
        }
        return false;
    }

    /**
     * Verifica se o ano é bissexto
     *
     * @param ano Ano a verificar.
     * @return true ou false.
     */
    public static boolean verificarAnoBissexto(int ano) {
        return (ano % 400 == 0) || ((ano % 4 == 0) && (ano % 100 != 0));
    }

    /**
     * Recebe uma string e verifica se é um numero inteiro positivo
     *
     * @param string String a verificar.
     * @return true se todos os caracteres forem digitos ou false
     */
    public static boolean isNumber(String string) {
        for (char c : string.toCharArray()) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Aux. Verifica se um email é válido ou não
     *
     * @param email - Email a verificar
     * @return - true ou false
     */
    public static boolean emailValido(String email) {
        String[] temp = email.split("@");
        if (temp.length != 2) {
            return false;
        } else {
            String[] temp2 = temp[1].split("\\.");
            if (temp2.length == 2 || temp2.length == 3) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Aux. Verifica se o jogo está carregado
     *
     * @param jogosId - Array com a lista dos IDs de jogos registados até ao momento.
     * @param idJogo - ID do jogo a verificar.
     * @param jogosCarregados - Número de jogos carregados até ao momento.
     * @return - Boolean que indica se o jogo existe ou não.
     */
    public static int jogoExiste(String[] jogosId, String idJogo, int jogosCarregados) {
        int jogoExiste = -1;
        for (int i = 0; i < (jogosCarregados); i++) {
            if (idJogo.equalsIgnoreCase(jogosId[i])) {
                jogoExiste = i;
            }
        }
        return jogoExiste;
    }

    /**
     * Aux. Muda de página
     *
     * @param i - Index do número de participantes.
     * @return - true se mudar de página.
     */
    public static boolean mudarDePagina(int i) {
        return i % MAX_LINHAS_PAGINA == 0;
    }

    /**
     * Verifica se um email já está registado.
     * 
     * @param tempEmail - Email a verificar.
     * @param nParticipantes - Número de participantes inscritos no evento.
     * @param participantes - Matriz com a informação dos participantes.
     * @return -1 se não existe linha com esse valor ou o nº da linha cujo primeiro elemento é esse valor
     */
    public static int pesquisarEmail(String tempEmail, int nParticipantes, String[][] participantes) {
        int teste = -1;
        for (int i = 0; i < nParticipantes; i++) {
            if (participantes[i][0].equals(tempEmail)) {
                teste = i;
            }
        }
        return teste;
    }

    /**
     * Repõe todos os valores da matriz dos prémios para 0.
     * 
     * @param premios - Matriz dos prémios.
     */
    public static void resetMatrizPremios(double[][] premios) {
        for (int i = 0; i < premios.length; i++) {
            for (int k = 0; k < premios[0].length; k++) {
                premios[i][k] = 0;
            }
        }
    }

}
