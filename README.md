# GameLand #

## Aplicação final APROG 2018
* Gonçalo Corte Real (1180793)

### Funcionalidades ###

* Estrutura de menu e métodos relacionados (Menu.class)
* Classe com métodos utilitários gerais (Utilitarios.class)
* Estrutura da aplicação (Main.class)
* Classe com métodos para registo de erros num ficheiro (LogErros.class)
* Funcionalidade 1: Carregar informação de jogos
* Funcionalidade 2: Ver detalhes sobre os jogos
* Funcionalidade 3: Ler ficheiro de inscrição de equipa
* Funcionalidade 4: Ver detalhes dos participante
* Funcionalidade 5: Alterar dados de um participante
* Funcionalidade 6: Carregar resultados de jogo
* Funcionalidade 7: Fazer backup da informação
* Funcionalidade 8: Calcular prémios
* Funcionalidade 9: Remover equipa
* Funcionalidade 10: Ver prémios por equipa
* Funcionalidade 11: Ver informação de jogo
* Funcionalidade 12: Ver listagem de prémios
* Testes Finais para resolução de possíveis bugs